{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import qualified Data.Vector.Unboxed as U
import Utilities

main :: IO ()
main =
    makeApp
        ( \( gid :: Int
            , n :: Int
            , l1 :: Double
            , l2 :: Double
            , s :: Double
            , _ :: Double -- s2 :: Double
            , _ :: Double -- s3 :: Double
            , _ :: Double -- s3k :: Double
            , y :: Double
            , y_trt :: Double
            , y_ntrt :: Double
            ) ->
                pure (gid, y, y_trt, y_ntrt, s, n, U.fromList [l1, l2])
        )
