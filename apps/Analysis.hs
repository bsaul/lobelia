{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import qualified Data.Vector.Unboxed as U
import Utilities

{-
per Kayla on 20230727 variables in dataset are{}

• cluster (cluster ID)
• S (proportion of treated kids)
• NS (proportion of untreated kids)
• Y_all (proportion of all kids with malaria)
• Y_trt (proportion of treated kids with malaria)
• Y_untrt (proportion of untreated kids with malaria)
• nkids (number of kids in the cluster,
    used as weights in models for S/NS and as "Nval" in the for loops in my code)
• avgbuilding (covariate)
• urban (covariate)
• avgalt (covariate)
• avgage (covariate)
• propwomen (covariate)
• avgtemp (covariate)
• avgrain (covariate)
• avgagdensity (covariate)

-}

main :: IO ()
main =
    makeApp
        ( \( cluster :: Int
            , s :: Double
            , _ :: Double -- ns :: Double
            , y_all :: Double
            , y_trt :: Double
            , y_untrt :: Double
            , n :: Int
            , --- covariates
              avgbuilding :: Double
            , urban :: Double
            , avgalt :: Double
            , avgage :: Double
            , propwomen :: Double
            , avgtemp :: Double
            , avgrain :: Double
            , avgagdensity :: Double
            ) ->
                pure
                    ( cluster
                    , y_all
                    , y_trt
                    , y_untrt
                    , s
                    , n
                    , U.fromList
                        [ avgbuilding
                        , urban
                        , avgalt
                        , avgage
                        , propwomen
                        , avgtemp
                        , avgrain
                        , avgagdensity
                        ]
                    )
        )
