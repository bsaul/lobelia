#!/usr/bin/env Rscript
#-------------------------------------------------------------------------------
# Script which *runs* simulations
# Rscript 'scripts/run_simulations.R' cabal
# unable to get following to work
# Rscript scripts/run_simulations.R $(nix eval --raw .#defaultPackage.x86_64-linux)/bin/simulation
args = commandArgs(trailingOnly=TRUE)

future::plan("multicore")

source("src/estimator.R")
source("src/utilities.R")
source("simulations/simulator.R")

# print(args)
# testing <- TRUE
testing <- FALSE

# do_simulation("simulations/testing.yaml", executeBy = "cabal", testing = TRUE)
# sims_to_run <- "testing"
sims_to_run <- args[-1]

purrr::walk(
  .x = sims_to_run,
  .f = ~ {
    do_simulation(
        paste0("simulations/", .x, ".yaml"), 
        executeBy = { if (testing) "cabal" else args[1] },
        testing = testing,
        checkbias = FALSE)
  }
)


