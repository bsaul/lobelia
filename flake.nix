{
  description = "Lobelia: G-estimation with partial interference";
  nixConfig = { bash-prompt = "λ "; };
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
    flake-utils.url = "github:numtide/flake-utils";
    gitignore = {
      url = "github:hercules-ci/gitignore.nix";
      # Use the same nixpkgs
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nix-filter.url = "github:numtide/nix-filter";
    packeraLib.url =
      "gitlab:bsaul/packera?rev=47138927727fb2d1e3b29417619fda2424bd96e9";
  };

  outputs = { self, nixpkgs, flake-utils, gitignore, nix-filter, packeraLib }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        packageName = "lobelia";

        pkgs = nixpkgs.legacyPackages.${system};

        filter = nix-filter.lib;

        pckra = pkgs.haskellPackages.callCabal2nix "packera" packeraLib { };

        inherit (gitignore.lib) gitignoreSource;

        rticles = pkgs.rPackages.buildRPackage {
          name = "rticles";
          buildInputs = with pkgs.rPackages; [
            pkgs.R
            knitr
            lifecycle
            tinytex
            rmarkdown
            xfun
            yaml
          ];
          src = pkgs.fetchFromGitHub {
            owner = "rstudio";
            repo = "rticles";
            rev = "2612baf2b09b65d7a293262642b460d6c93cdfb1";
            sha256 = "lPn7vZgtp4seeCCkMQKGluTiaoeIbKHKRyG3GEtPv3o=";
          };
        };

        tex = pkgs.texlive.combine {
          inherit (pkgs.texlive)
            scheme-medium xcolor booktabs etoolbox titling framed multirow
            sttools # contains cuted?
            changepage dblfloatfix varwidth ulem wrapfig
            # rotating
            # enumitem
            soul stix mathastext boites
            endfloat chngcntr
            # floatpag
            # calc
            # ifpdf
          ;
        };

        RandPkgs = [
          # R and packages
          pkgs.R
          pkgs.rPackages.assertthat
          pkgs.rPackages.bookdown
          pkgs.rPackages.broom
          pkgs.rPackages.devtools
          pkgs.rPackages.digest
          pkgs.rPackages.dplyr
          pkgs.rPackages.geeM
          pkgs.rPackages.geex
          pkgs.rPackages.git2r
          pkgs.rPackages.ggplot2
          pkgs.rPackages.glue
          pkgs.rPackages.gt
          pkgs.rPackages.furrr
          pkgs.rPackages.here
          pkgs.rPackages.httpgd
          pkgs.rPackages.jsonlite
          pkgs.rPackages.languageserver
          pkgs.rPackages.lintr
          rticles
          pkgs.rPackages.report
          pkgs.rPackages.usethis
          pkgs.rPackages.styler
          pkgs.rPackages.tidyr
          pkgs.rPackages.testthat
          pkgs.rPackages.yaml

          # Documentation/writing tools
          pkgs.pandoc
        ];

        mkSimulation = name : sims : pkgs.stdenv.mkDerivation {
          name = name ;
          src = filter {
            root = ./.;
            include = [
              "src"
              "simulations/simulator.R"
              "simulations/run_simulations.R"
            ] ++ 
              map (x : "simulations/${x}.yaml") sims
            ;
          };
          buildInputs = [
            pkgs.blas
            pkgs.lapack
            pkgs.gsl
            self.packages.${system}.${packageName}
          ] ++ RandPkgs;
          buildPhase = ''
            mkdir -p results/
            ${pkgs.R}/bin/Rscript simulations/run_simulations.R ${
              self.packages.${system}.${packageName}
            }/bin/simulation ${pkgs.lib.strings.concatStringsSep " " sims}
          '';
          installPhase = ''
            mkdir -p $out
            cp -r results/ $out/
          '';
        };

      in {

        formatter = pkgs.nixfmt;

        # effectively: `cabal build`
        packages.${packageName} = pkgs.haskellPackages.callCabal2nix packageName
          (filter {
            root = ./.;
            include = [
              "src"
              "apps"
              "cabal.project"
              "lobelia.cabal"
              "LICENSE"
              "LICENSE.md"
            ];
          }) rec { packera = pckra; };

        # Run the analysis
        packages.analysis = pkgs.stdenv.mkDerivation {
          name = "analysis";
          src = filter {
            root = ./.;
            include = [ "src" "scripts" "data" ];
          };
          buildInputs = [
            pkgs.blas
            pkgs.lapack
            pkgs.gsl
            self.packages.${system}.${packageName}
          ] ++ RandPkgs;
          buildPhase = ''
            mkdir -p results/
            ${pkgs.R}/bin/Rscript scripts/run_analysis.R ${
              self.packages.${system}.${packageName}
            }/bin/analysis
          '';
          installPhase = ''
            mkdir -p $out
            cp -r results/ $out/
          '';
        };

        # Run the simulations presented in the main text
        packages.simulations.mainText = 
          mkSimulation
            "simulations"
            [ "linear" 
              "spline" 
            ];

        # Run the simulations presented in the appendix text
        packages.simulations.appendix = 
          mkSimulation
            "simulations-appendix"
            [ "app-linear-correct" 
              "app-linear-out-mis" 
              "app-linear-trt-mis"
              "app-linear-both-mis"
              "app-cubic-correct"
              "app-cubic-out-mis" 
              "app-cubic-trt-mis" 
              "app-cubic-both-mis"
            ];

        # Simulation definition used for testing/debugging
        # List the result files: 
        # ll $(nix eval --raw .#simulations.testing)/results
        packages.simulations.testing = 
          mkSimulation 
            "simulations-test" 
            ["app-linear-trt-mis"] ;

        # Render the manuscript
        packages.paper = pkgs.stdenv.mkDerivation {
          name = "paper";
          # TODO only include needed files to avoid unnecessary builds
          src = gitignoreSource ./.;
          # https://discourse.nixos.org/t/fontconfig-error-no-writable-cache-directories/34447
          buildInputs = RandPkgs ++ [ tex pkgs.rPackages.cowplot ];
          buildPhase = ''
            cd manuscript
            export XDG_CACHE_HOME="$(mktemp -d)" 
            ${pkgs.R}/bin/Rscript --vanilla -e \
                'bookdown::render_book(".", output_format = "bookdown::html_document2", output_dir = "html", params = list(rev_info = "${
                  if (self ? rev) then self.rev else "dirty"
                }", results_path = "${
                  self.packages.${system}.analysis
                }", sim_path = "${
                  self.packages.${system}.simulations.mainText
                }", appendix_sim_path = "${
                  self.packages.${system}.simulations.appendix
                }"))'

            ${pkgs.R}/bin/Rscript --vanilla -e \
                'bookdown::render_book(".", output_format = "bookdown::pdf_book", output_dir = "pdf", params = list(rev_info = "${
                  if (self ? rev) then self.rev else "dirty"
                }", results_path = "${
                  self.packages.${system}.analysis
                }", sim_path = "${
                  self.packages.${system}.simulations.mainText
                }", appendix_sim_path = "${
                  self.packages.${system}.simulations.appendix
                }"))'

          '';
          installPhase = ''
            # ls
            mkdir -p $out
            cp -r pdf/_main.pdf $out/lobelia-${
              if (self ? rev) then builtins.substring 0 6 self.rev else "dirty"
            }.pdf
            cp -r pdf/_main.tex $out/lobelia-${
              if (self ? rev) then builtins.substring 0 6 self.rev else "dirty"
            }.tex
            cp -r _main.html $out/lobelia-${
              if (self ? rev) then builtins.substring 0 6 self.rev else "dirty"
            }.html
          '';
        };

        # Create a docker image with the analysis application
        packages.docker = pkgs.dockerTools.buildLayeredImage {
          name = "lobelia-analysis";
          contents = [ pkgs.blas pkgs.lapack pkgs.gsl ];
          config = {
            EntryPoint =
              [ "${self.packages.${system}.${packageName}}/bin/analysis" ];
          };
        };

        # run the simulation app
        # nix run .#simulation
        apps.simulation = {
          type = "app";
          program = "${self.packages.${system}.${packageName}}/bin/simulation";
        };

        # run the analysis app
        # nix run .#analysis
        apps.analysis = {
          type = "app";
          program = "${self.packages.${system}.${packageName}}/bin/analysis";
        };

        defaultPackage = self.packages.${system}.${packageName};

        # Development shell
        devShells.default = pkgs.mkShell {
          buildInputs = [
            pkgs.nixfmt
            pkgs.treefmt

            # docker
            pkgs.docker

            # Haskell
            pkgs.haskellPackages.haskell-language-server
            pkgs.haskellPackages.ghcid
            pkgs.haskellPackages.cabal-install
            pkgs.haskellPackages.fourmolu

            # System dependencies for hmatrix/hmatrix-gsl
            pkgs.blas
            pkgs.lapack
            pkgs.gsl

            # Latex + packages
            tex

          ] ++ RandPkgs ++ [ pkgs.rPackages.cowplot ];
        };
      });
}
