{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE FlexibleContexts #-}

module Lobelia where

import qualified Data.Vector as V
import Data.Vector.Generic as G
import qualified Data.Vector.Unboxed as U
import Numeric.AD.Double
import Numeric.GSL.Root
import qualified Numeric.LinearAlgebra as LA
import Packera
import Statistics.Distribution
import Statistics.Distribution.Binomial

{- |
What should 'estimator' return?
-}
data ComputationGoal
    = -- \^ Return the Parameter Estimates (point and variance)

      -- | Return the evaluated estimating equations (i.e sum of psi(theta, O))
      --   Useful for debugging the psi functions to be sure they are unbiased.
      ParameterEstimates
    | EstimatingEqns

{- |
Target Estimand
-}
data TargetEffect
    = Overall
    | Treated
    | Untreated

{- |
Robustness of the estimator
-}
data Robustness = Single | Double

{- |
Define the available f(S) functions
-}
data STransform
    = None
    | Quadratic
    | Cubic
    | Quartic
    | CubicSpline [Double]
    | Piecewise [Double]

{- |
Pairs of (start, length) corresponding to the locations of parameters
in the "theta" vector.
-}
data ParameterMap = MkParameterMap
    { rho :: (Int, Int)
    , beta :: (Int, Int)
    , psi :: (Int, Int)
    , nu :: (Int, Int)
    }

{- |
Estimating equation function
for weighted logistic regression.
In the paper,
this is the model for:

\[
e(L ; \rho)
\]
-}
psiLogistic
    :: ( Vector v (Scalar c)
       , Vector v c
       , Mode c
       , Floating c
       )
    => (Scalar c, Int, v (Scalar c)) -- data: (s , n , l1 ++ l2)
    -> v c -- rho parameters
    -> v c
psiLogistic (s, n, l) ρ =
    let !xs = withIntercept (G.map auto l)
     in G.map ((auto s - logistic (xs <.> ρ)) * fromIntegral n *) xs
{-# INLINE psiLogistic #-}

{- |
Estimating function
-}
psiG
    :: ( Vector v (Scalar c)
       , Vector v c
       , Mode c
       , Floating c
       , Real c
       )
    => TargetEffect
    -> STransform -- the f(S) function
    -> Robustness
    -> [Scalar Double] -- s values
    -> ParameterMap
    -> c
    -> (Scalar Int, Scalar c, Scalar c, Scalar Int, v (Scalar c)) -- the data
    -> v c -- parameters; the "theta" vector
    -> v c
psiG effect stform robust svals pos z (_, y, s, n, l) θ =
    let !(fs, efs) = mkSTransform stform n

        !sv = G.fromList $ Prelude.map realToFrac svals

        getP = uncurry slice
        !ρ = getP (rho pos) θ
        !β = getP (beta pos) θ
        !ψ = getP (psi pos) θ
        !ν = getP (nu pos) θ

        -- Score functions for rho
        !ρscores = psiLogistic (s, n, l) ρ
        !ls = G.map auto l
        !fSs = fs (auto s)

        -- We need E[S | L;ρ] (fitted values from e_model)
        !eSL = logistic (withIntercept ls <.> ρ)
        -- in order to compute E[f(S) | L;ρ]
        !efSL = efs eSL

        !dr = case robust of
            Single -> G.empty
            Double -> ls
        -- Linear combinations corresponding with β and ψ
        !ηβ = withIntercept (efSL G.++ dr) <.> β
        !ηψ = fSs <.> ψ

        -- Score functions for beta
        !βscores =
            G.map
                (((auto y * exp (negate (ηβ + ηψ))) - 1) *)
                (withIntercept (efSL G.++ dr))

        -- Score functions for psi
        !g = (auto y * exp (negate ηψ)) - exp ηβ
        !ψscores = G.map (exp (negate ηβ) * g *) (G.zipWith (-) fSs efSL)

        -- Target estimating functions
        !targets = psiTarget effect fs sv ψ z ν
     in ρscores
            G.++ βscores
            G.++ ψscores
            G.++ targets
{-# INLINE psiG #-}

{- |
Estimating function for target parameters
-}
psiTarget
    :: (Vector v a, Floating a, Ord a)
    => TargetEffect
    -> (a -> v a)
    -- ^ spline knot setting
    -> v a
    -- ^ values at which to evaluate target
    -> v a
    -- ^ psi parameters
    -> a
    -> v a
    -> v a
psiTarget effect f svals ψ z = G.zipWith (-) (makeCounterfactual effect f svals ψ z)
{-# INLINE psiTarget #-}

{- |
Create counterfactual ratios
-}
makeCounterfactual
    :: (Floating a, Vector v a, Ord a)
    => TargetEffect
    -> (a -> v a)
    -> v a
    -> v a
    -> a
    -> v a
makeCounterfactual effect f sval ψ z =
    G.map (\x -> exp (G.zipWith (-) (f (g effect x)) (f (g effect z)) <.> ψ)) sval
  where
    g Overall x = x
    g Treated x = 1 - x
    g Untreated x = x
{-# INLINE makeCounterfactual #-}

{- |
Root finding function
-}
rootFinder :: [Double] -> (U.Vector Double -> U.Vector Double) -> U.Vector Double
rootFinder start f =
    U.fromList
        ( fst
            ( root
                DNewton
                0.0000001
                100
                (U.toList . f . U.fromList)
                start
            )
        )

{- |
HACKY way to gather necessary data.
NOTE that psi function expects:

* i = cluster id
* y = outcome
* s = proportion treated (or untreated depending on the effect)
* n = number of subjects in cluster
* v = vector of covariates
-}
toAnalyisData
    :: TargetEffect
    -> (Int, Double, Double, Double, Double, Int, U.Vector Double)
    -> (Int, Double, Double, Int, U.Vector Double)
toAnalyisData Overall (i, y_all, _, _, s, n, v) = (i, y_all, s, n, v)
toAnalyisData Treated (i, _, y_trt, _, s, n, v) = (i, y_trt, 1 - s, n, v)
toAnalyisData Untreated (i, _, _, y_ntrt, s, n, v) = (i, y_ntrt, s, n, v)

{- |
The main estimation routine
-}
estimator
    :: (Functor f, Foldable f)
    => STransform
    -> Robustness
    -> ParameterMap
    -> [Double] -- values of `s` at which to estimate counterfactual ratio
    -> [Double] -- starting roots
    -> Double -- value of `s` for the counterfactual constrast
    -> TargetEffect
    -> f (Int, Double, Double, Double, Double, Int, U.Vector Double)
    -> Result Double
estimator strm robust pmap svals rts cfactS effect =
    let psie
            :: ( Vector v (Scalar c)
               , Vector v c
               , Mode c
               , Floating c
               , Real c
               )
            => c
            -> (Int, Scalar c, Scalar c, Int, v (Scalar c))
            -> v c
            -> v c
        psie =
            psiG
                effect
                strm
                robust
                svals
                pmap
     in mestimate
            -- (GivenRoots $ G.fromList rts)
            (FindRoots $ rootFinder rts)
            ( Jacobian
                ( \xs ->
                    jacobian
                        ( convert
                            . psiSumF
                                ( \(gid, y, s, n, l) ->
                                    psie (auto cfactS) (gid, y, s, n, convert l)
                                )
                                (V.map auto (V.replicate (Prelude.length rts) 0.0))
                                xs
                        )
                        . convert
                )
            )
            (psie (auto cfactS))
            . fmap (toAnalyisData effect)
{-# INLINE estimator #-}

{-
Function to check the values of the evaluated estimating equation
-}
checkee
    :: (Functor f, Foldable f)
    => STransform
    -> Robustness
    -> ParameterMap
    -> [Double] -- values of `s` at which to estimate counterfactual ratio
    -> [Double] -- starting roots
    -> Double -- value of `s` for the counterfactual constrast
    -> TargetEffect
    -> f (Int, Double, Double, Double, Double, Int, U.Vector Double)
    -> U.Vector Double
checkee strm robust pmap svals rts cfactS effect =
    let psie
            :: ( Vector v (Scalar c)
               , Vector v c
               , Mode c
               , Floating c
               , Real c
               )
            => c
            -> (Int, Scalar c, Scalar c, Int, v (Scalar c))
            -> v c
            -> v c
        psie =
            psiG
                effect
                strm
                robust
                svals
                pmap
     in psiSum
            ( psie (auto cfactS)
                . (\(gid, y, s, n, l) -> (gid, y, s, n, convert l))
                . toAnalyisData effect
            )
            (fromList rts)
{-# INLINE checkee #-}

{-
  Utilities, etc
-}

-- Extract results
getResults :: [Double] -> Result Double -> [(Double, Double, Double)]
getResults svals x =
    let p = Prelude.length svals
        estimates = roots x
        n = Prelude.length estimates
        stderrs = case covariance x of
            Nothing -> [0.0]
            Just v -> Prelude.map sqrt $ LA.toList $ LA.takeDiag $ sandwich v
        es = Prelude.zip estimates stderrs
     in Prelude.zipWith (\s (e, std) -> (s, e, std)) svals (Prelude.drop (n - p) es)

-- Logistic function
logistic :: (Num a, Floating a) => a -> a
logistic x = 1 / (1 + exp (negate x))
{-# INLINE logistic #-}

-- Dot Product
(<.>) :: (Vector v a, Num a) => v a -> v a -> a
xs <.> ys = G.sum (G.zipWith (*) xs ys)

-- Prepend 1.0 to a vector
withIntercept :: (Vector v a, Num a) => v a -> v a
withIntercept = G.cons 1

-- Binomial density
dbinom :: Int -> Double -> Int -> Double
dbinom n p x = (`probability` x) (binomial n p)

-- Utilities for working with f(S) functions
mkSfunctions
    :: (Vector v a, Floating a, Real a)
    => [a -> Double]
    -> Int
    -> (a -> v a, a -> v a)
mkSfunctions fs n =
    ( \x -> G.fromList $ fmap (\g -> realToFrac (g x)) fs
    , \x -> G.fromList $ fmap ((\g -> g x) . conditionalize n) fs
    )
{-# INLINE mkSfunctions #-}

-- Create pair of functions corresponding to f(S) and E[f(S) | L ]
mkSTransform
    :: (Vector v a, Floating a, Real a)
    => STransform
    -> Int
    -> (a -> v a, a -> v a)
mkSTransform None = \n -> (singleton, singleton . conditionalize n id)
mkSTransform Quadratic =
    mkSfunctions
        [ realToFrac . (** 1)
        , realToFrac . (** 2)
        ]
mkSTransform Cubic =
    mkSfunctions
        [ realToFrac . (** 1)
        , realToFrac . (** 2)
        , realToFrac . (** 3)
        ]
mkSTransform Quartic =
    mkSfunctions
        [ realToFrac . (** 1)
        , realToFrac . (** 2)
        , realToFrac . (** 3)
        , realToFrac . (** 4)
        ]
mkSTransform (CubicSpline knots) =
    mkSfunctions
        ( [ realToFrac . (** 1)
          , realToFrac . (** 2)
          , realToFrac . (** 3)
          ]
            Prelude.++ ((\k x -> posPart (realToFrac x - realToFrac k) ** 3) <$> knots)
        )
  where
    posPart x = if x > 0 then x else 0.0
mkSTransform (Piecewise knots) =
    mkSfunctions
        ( ( \(a, b) x ->
                if realToFrac a < x && x <= realToFrac b then 1.0 else 0.0
          )
            <$> knotsToPieces knots
        )

--
conditionalize
    :: (Fractional a, Fractional t, Real a)
    => Int
    -> (t -> Double)
    -> a
    -> a
conditionalize n f x =
    realToFrac $
        V.sum $
            G.map
                ( \i ->
                    f (fromIntegral i / fromIntegral n)
                        * dbinom n (realToFrac x) i
                )
                (G.enumFromN 0 (n + 1))
{-# INLINE conditionalize #-}

-- Convert a list of "knots" to ordered pairs
knotsToPieces :: [Double] -> [(Double, Double)]
knotsToPieces x =
    Prelude.zip (Prelude.take (Prelude.length x - 1) x) (Prelude.tail x)
