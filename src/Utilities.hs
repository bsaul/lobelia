{-# LANGUAGE MultiWayIf #-}

module Utilities where

import qualified Data.Aeson as A
import qualified Data.ByteString.Lazy as BL
import Data.Csv hiding (Parser)
import qualified Data.Vector as V
import qualified Data.Vector.Unboxed as U
import Lobelia
import Options.Applicative as O

data Input
    = FileInput FilePath
    | StdInput

data EstimatorControl = MkEstimatorControl
    { input :: !Input
    , svals :: ![Double]
    , scontrast :: !Double
    , stransform :: !STransform
    , robustness :: !Robustness
    , theta :: !ThetaStart
    , targetEffect :: !TargetEffect
    , computationGoal :: ComputationGoal
    }

estimatorControl :: Parser EstimatorControl
estimatorControl =
    MkEstimatorControl
        <$> (fileInput <|> stdInput)
        <*> option
            auto
            ( short 's'
                <> metavar "VALUES"
                <> help "values of s at which to compute target"
            )
        <*> option
            auto
            ( long "scontrast"
                <> value 0.55
                <> showDefault
                <> metavar "VALUE"
                <> help "value of s at which constrast"
            )
        <*> ( flag' None (short 'n' <> help "no transform of s")
                <|> flag' Quadratic (long "quadratic" <> help "quadratic transform of s")
                <|> flag' Cubic (long "cubic" <> help "cubic transform of s")
                <|> flag' Quartic (long "quartic" <> help "quartic transform of s")
                <|> CubicSpline
                <$> option
                    auto
                    ( long "cubicSpline"
                        <> metavar "KNOTS"
                        <> help "cubic spline transform of s"
                    )
                    <|> Piecewise
                <$> option
                    auto
                    ( long "piecewise"
                        <> metavar "KNOTS"
                        <> help "piecewise transform of s"
                    )
            )
        <*> ( flag' Single (long "sr" <> help "singly robust")
                <|> flag' Double (long "dr" <> help "doubly roubust")
            )
        <*> thetaStart
        <*> ( flag' Overall (long "overall" <> help "Overall Effect")
                <|> flag' Treated (long "treated" <> help "Spillover in the treated")
                <|> flag' Untreated (long "untreated" <> help "Spillover in the untreated")
            )
        <*> flag
            ParameterEstimates
            EstimatingEqns
            (long "checkee" <> help "Evaluate Estimating Eqns for debugging")

opts :: ParserInfo EstimatorControl
opts =
    info
        (estimatorControl <**> helper)
        ( fullDesc
            <> progDesc "Run the Lobelia estimator"
            <> O.header "Lobelia estimator"
        )

fileInput :: Parser Input
fileInput =
    FileInput
        <$> strOption
            ( long "file"
                <> short 'f'
                <> metavar "FILENAME"
                <> help "Input file"
            )

stdInput :: Parser Input
stdInput =
    flag'
        StdInput
        ( long "stdin"
            <> help "Read from stdin"
        )

data ThetaStart = MkThetaStart
    { rho' :: [Double]
    , beta' :: [Double]
    , psi' :: [Double]
    }

thetaStart :: Parser ThetaStart
thetaStart =
    MkThetaStart
        <$> option
            auto
            ( long "rho"
                <> metavar "RHO"
                <> help "initial values of rho"
            )
        <*> option
            auto
            ( long "beta"
                <> metavar "BETA"
                <> help "initial values of beta"
            )
        <*> option
            auto
            ( long "psi"
                <> metavar "PSI"
                <> help "initial values of psi"
            )

mkParameterMap :: ThetaStart -> Int -> ParameterMap
mkParameterMap (MkThetaStart ro bt ps) nnu =
    let nrho = length ro
        nbeta = length bt
        npsi = length ps
     in MkParameterMap
            { rho = (0, nrho)
            , beta = (nrho, nbeta)
            , psi = (nrho + nbeta, npsi)
            , nu = (nrho + nbeta + npsi, nnu)
            }

checkOptions :: EstimatorControl -> Either String EstimatorControl
checkOptions x =
    let nrho = length (rho' $ theta x)
        nbeta = length (beta' $ theta x)
        npsi = length (psi' $ theta x)
        badPsi = case stransform x of
            None -> npsi /= 1
            Quadratic -> npsi /= 2
            Cubic -> npsi /= 3
            Quartic -> npsi /= 4
            CubicSpline knots -> npsi /= 3 + length knots
            Piecewise knots -> npsi /= length knots - 1
        badBeta = case robustness x of
            Single -> nbeta /= npsi + 1
            Double -> nbeta - npsi /= nrho
     in if
            | badPsi ->
                Left $
                    "psi should have length of "
                        ++ ( case stransform x of
                                None -> "1"
                                Quadratic -> "2"
                                Cubic -> "3"
                                Quartic -> "4"
                                CubicSpline knots -> show (3 + length knots)
                                Piecewise knots -> show (length knots - 1)
                           )
            | null (svals x) -> Left "must have at least one value of s"
            | badBeta ->
                Left $
                    "beta should have length of "
                        ++ ( case robustness x of
                                Single -> "psi + 1"
                                Double -> "rho (number of parameters) + psi"
                           )
            | otherwise -> Right x

rootStart :: ThetaStart -> Int -> [Double]
rootStart x n = rho' x ++ beta' x ++ psi' x ++ Prelude.replicate n 1

makeApp
    :: (FromRecord a)
    => (a -> IO (Int, Double, Double, Double, Double, Int, U.Vector Double))
    -> IO ()
makeApp f = do
    p <- checkOptions <$> execParser opts
    case p of
        Left err -> putStrLn err
        Right options -> do
            csvData <- case input options of
                FileInput x -> BL.readFile x
                StdInput -> BL.getContents
            let pmap = mkParameterMap (theta options) (Prelude.length (svals options))
            case decode HasHeader csvData of
                Left err -> putStrLn err
                Right v ->
                    case computationGoal options of
                        ParameterEstimates ->
                            do
                                BL.putStr
                                . A.encode
                                . getResults (svals options)
                                . estimator
                                    (stransform options)
                                    (robustness options)
                                    pmap
                                    (svals options)
                                    (rootStart (theta options) $ Prelude.length (svals options))
                                    (scontrast options)
                                    (targetEffect options)
                                =<< V.forM v f
                        EstimatingEqns ->
                            do
                                BL.putStr
                                . A.encode
                                . checkee
                                    (stransform options)
                                    (robustness options)
                                    pmap
                                    (svals options)
                                    (rootStart (theta options) $ Prelude.length (svals options))
                                    (scontrast options)
                                    (targetEffect options)
                                =<< V.forM v f
