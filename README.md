---
title: "Lobelia: G-estimation with partial interference"
languages: [nix, R]
---

## Working on the project

1. Clone the repo: `git clone git@gitlab.com:bsaul/lobelia.git`.
2. Create a branch to make your changes, e.g.:
`git checkout -b make-some-changes`.
Here, `make-some-changes` is the name of your new branch.
3. Make the changes to the files.
4. Commit your changes, e.g.:
`git commit -am "Some brief desciption of changes"`.
5. Push your changes to the remote repository:
`git push`.
Depending on your settings and/or version of `git`,
you may need to set the upstream branch, as in:
`git branch --set-upstream make-some-changes origin/make-some-changes`
6. Create a [merge request](https://gitlab.com/bsaul/lobelia/-/merge_requests)
with your new changes.
If your changes need review, assign a review.
Otherwise, you can merge to the `main` branch without a review
by clicking the Merge button on the merge request's page.
